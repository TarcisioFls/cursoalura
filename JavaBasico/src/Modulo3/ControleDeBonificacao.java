package Modulo3;

public class ControleDeBonificacao {
	
	private double totalDeBonificacao = 0;
	
	public void registra(Funcionario funcionario) {
		System.out.println("Adicionando bonificação do funcionario: " + funcionario.getNome());
		this.totalDeBonificacao += funcionario.getBonificacao();
	}
	
	public double getTotalDeBonificacao() {
		return totalDeBonificacao;
	}

}
