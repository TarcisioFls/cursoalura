package Modulo5;

public class Retangulo implements AreaCalculavel{
	
	private int altura;
	private int largura;
	
	public Retangulo(int altura, int largura) {
		this.altura = altura;
		this.largura = largura;
	}

	@Override
	public double calcularArea() {
		return this.largura * this.altura;
	}
	

}
