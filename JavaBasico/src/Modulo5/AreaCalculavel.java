package Modulo5;

public interface AreaCalculavel {
	
	double calcularArea();

}
