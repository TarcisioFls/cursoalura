package Modulo5;

public class Teste {
	
	public static void main(String[] args) {
		AreaCalculavel retangulo = new Retangulo(4, 3);
		AreaCalculavel quadrado = new Quadrado(4);
		AreaCalculavel circulo = new Circulo(5);
		
		System.out.println("Area do retangulo: " + retangulo.calcularArea());
		System.out.println("Area do quadrado: " + quadrado.calcularArea());
		System.out.println("Area do circulo: " + circulo.calcularArea());
	}

}
