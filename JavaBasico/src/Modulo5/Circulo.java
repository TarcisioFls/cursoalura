package Modulo5;

public class Circulo implements AreaCalculavel{
	
	private int raio;
	
	public Circulo(int raio) {
		this.raio = raio;
	}

	@Override
	public double calcularArea() {
		return Math.PI * (this.raio * this.raio);
	}

}
