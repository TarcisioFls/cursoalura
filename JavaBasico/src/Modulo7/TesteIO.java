package Modulo7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class TesteIO {
	
	public static void main(String[] args) throws IOException {

		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(
						System.in));
		Scanner entrada = new Scanner(bufferedReader);
		
		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(
						new FileOutputStream("saida.txt")));
		
		while (entrada.hasNextLine()) {
			
			String linha = entrada.nextLine();
			writer.write(linha);
			writer.newLine();
		}
		
		bufferedReader.close();
		entrada.close();
		writer.close();
		
	}

}