package Modulo7;

import java.util.Collection;
import java.util.HashSet;

import Modulo2.Conta;
import Modulo2.ContaCorrente;

public class TesteHasCode {

	public static void main(String[] args) {
		
		Conta conta = new ContaCorrente();
		Collection	<Conta> contas = new HashSet<Conta>();
		
		conta.setNumero(1);
		conta.setNome("Mario");
		
		Conta conta2 = new ContaCorrente();
		
		conta2.setNumero(1);
		conta2.setNome("Mario");
		
		Conta conta3 = new ContaCorrente();
		
		conta3.setNumero(2);
		conta3.setNome("Maria");
		
		contas.add(conta);
		contas.add(conta2);
		contas.add(conta3);
		
		System.out.println(contas);

	}

}
