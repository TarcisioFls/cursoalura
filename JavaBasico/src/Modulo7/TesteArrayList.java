package Modulo7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TesteArrayList {

	public static void main(String[] args) {

		List<Integer> numeros = new ArrayList<Integer>();
		
		for (int i = 1 ; i <= 1000 ; i++) {
			numeros.add(i);
		}
		
		Collections.reverse(numeros);
		
		for (Integer ol : numeros) {
			System.out.println(ol);
		}
		
	}

}
