package Modulo7;

import java.util.TreeSet;

public class TesteTreeSet {

	public static void main(String[] args) {

		TreeSet<Integer> numeros = new TreeSet<>();
		
		for (int i = 1 ; i <= 1000 ; i++) {
			
			numeros.add(i);
			
		}
		
		for (Integer integer : numeros.descendingSet()) {
			System.out.println(integer + "");
		}
		
	}

}
