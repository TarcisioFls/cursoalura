package Modulo7;

import java.util.HashMap;
import java.util.Map;

import javax.swing.text.MaskFormatter;

import Modulo2.Conta;
import Modulo2.ContaCorrente;

public class TesteExercicio12Colletions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conta c1 = new ContaCorrente();
		c1.deposita(10000);
		
		Conta c2 = new ContaCorrente();
		c2.deposita(3000);
		
		Map<String, Conta> mapasDeContas =  new HashMap<String, Conta>();
		
		mapasDeContas.put("Diretor", c1);
		mapasDeContas.put("Gerente", c2);
		
		Conta contaDoDiretor = mapasDeContas.get("Direitor");
		
	}

}
