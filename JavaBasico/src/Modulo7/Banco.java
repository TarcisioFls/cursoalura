package Modulo7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Modulo2.Conta;

public class Banco {
	
	private List<Conta> contas = new ArrayList<Conta>();
	
	private Map<String, Conta> map = new HashMap<String, Conta>();
	
	public void adiciona(Conta conta) {
		this.contas.add(conta);
		this.map.put(conta.getNome(), conta);	
	}
	
	public Conta pega (int posicao) {
		return contas.get(posicao);
	}
	
	public int quantidadeDeContas() {
		return contas.size();
	}
	
	public Conta nomeContas(String nome) {
		return map.get(nome);
	}

}
