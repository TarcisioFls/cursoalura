package Modulo7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TesteExercicio10Collection {

	public static void main(String[] args) {
		int tempo = 50000;
		List<Integer> numeros = new LinkedList<Integer>();
		long inicioFor1 = System.currentTimeMillis();
		
		for (int i = 1; i <= tempo; i++) {
			numeros.add(i);
		}
		
		long fimFor1 = System.currentTimeMillis();
		
//		long inicioFor2 = System.currentTimeMillis();
//		for (int i = 1; i <= tempo; i++) {
//			numeros.contains(i);
//		}
		
		long fimFor2 = System.currentTimeMillis();
		System.out.println("For 1 " + (fimFor1 - inicioFor1)/1000.0);
//		System.out.println("For 2 " + (fimFor2 - inicioFor2));

	}

}
