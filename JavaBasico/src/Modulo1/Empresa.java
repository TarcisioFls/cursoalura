package Modulo1;

public class Empresa {
	
	private String nome;
	private String cnpj;
	private Funcionario funcionario;
	private Funcionario[] funcionarios;
	
	public Empresa(int quantidadeFuncionario) {
		this.funcionarios = new Funcionario[quantidadeFuncionario];
	}
	
	void adiciona(Funcionario f) {
		for(int i = 0;i < funcionarios.length;i++) {
			if(funcionarios[i] == null) {
				this.funcionarios[i] = f;
				break;
			}
		}
	}
	
	void mostrarEmpregados() {
		for(int i = 0;i < funcionarios.length;i++) {
			System.out.println("Funcionario na posição " + i);
			System.out.println("Salário : R$" + funcionarios[i].getSalario() + "\n");
		}
	}
	
	void mostraTodasAsInformacoes() {
		for(int i = 0; i < funcionarios.length; i++) {
			System.out.println("Dados do funcionario na posição " + i);
			funcionarios[i].monstra();
		}
	}
	
	boolean contem(Funcionario funcionario) {
		for(int i = 0; i < funcionarios.length;i++) {
			if (funcionario == funcionarios[i]) {
				return true;
			} 
		}
		return false;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Funcionario[] getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Funcionario[] funcionarios) {
		this.funcionarios = funcionarios;
	}
}
