package Modulo1;

public class Capitulo2Exercicio8 {

	public static void main(String[] args) {
		int x = 13;
		
		while(x != 1) {
			if (x == 13) {
				System.out.print(x + " > ");
			}
			if(x % 2 == 0) {
				System.out.print(x/2);
				x = x/2;
			} else {
				System.out.print((3*x+1));
				x = (3*x+1);
			}
			if(x != 1) {
				System.out.print(" > ");
			}
		}

	}

}
