package Modulo1;


public class TesteFuncionario {

	public static void main(String[] args) {
//		Empresa empresa = new Empresa(1);
//		empresa.setFuncionario(new Funcionario());
//		
//		empresa.getFuncionario().setNome("Tarcísio");
//		empresa.getFuncionario().setDataEntrada(new Data());
//		empresa.getFuncionario().setDepartamento("TI");
//		empresa.getFuncionario().setRg("123456");
//		empresa.getFuncionario().setSalario(1800);
//		
//		empresa.getFuncionario().getDataEntrada().preencherData(06, 01, 2016);
//		
//		empresa.setFuncionarios(new Funcionario[1]);
//		empresa.adiciona(empresa.getFuncionario());
//		
//		
//		for(int i =0; i < empresa.getFuncionarios().length;i++) {
//			empresa.getFuncionarios()[i].monstra();
//		}
		
		Empresa empresa = new Empresa(1);
		
		for (Funcionario funcionario : empresa.getFuncionarios()) {
			funcionario = new Funcionario();
			funcionario.setNome("Tarcísio");
			funcionario.setDataEntrada(new Data(06, 01, 2016));
			//funcionario.getDataEntrada().preencherData(06, 01, 2016);
			funcionario.setDepartamento("TI");
			funcionario.setRg("123456");
			funcionario.setSalario(1800);
			funcionario.monstra();
		}
	}

}
