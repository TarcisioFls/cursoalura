package Modulo1;

public class TesteEmpresa {
	
	public static void main (String[] args) {
		Empresa empresa = new Empresa(2);
		
		Funcionario f1 = new Funcionario();
		f1.setNome("Tarcísio");
		f1.setDataEntrada(new Data(06, 01, 2016));
		f1.setDepartamento("TI");
		f1.setRg("123456");
		f1.setSalario(1800);		
//		f1.getDataEntrada().preencherData(06, 01, 2016);
		
		empresa.adiciona(f1);
		
		Funcionario f2 = new Funcionario();
		f2.setNome("Gabi");
		f2.setDataEntrada(new Data(01, 1, 2015));
//		f2.getDataEntrada().preencherData(01, 01, 2015);
		f2.setDepartamento("Infraestrutura");
		f2.setSalario(1800);;
		f2.setRg("54321");
		
		empresa.adiciona(f2);
		
		for (Funcionario funcionario : empresa.getFuncionarios()) {
			funcionario.monstra();
		}
				
		empresa.mostrarEmpregados();
		
		empresa.mostraTodasAsInformacoes();
		
		System.out.println(empresa.contem(f1));
	}

}
