package Modulo1;

public class Funcionario {
	
	private String nome;
	private String departamento;
	private double salario;
	private Data dataEntrada;
	private String rg;
	private static int identificador = 1;
	
	public Funcionario(String nome) {
		this.nome = nome;
		this.identificador ++;
	}
	
	public Funcionario() {
		this.identificador ++;
	}
	
	public void recebeAumento(double aumento) {
		this.salario += aumento;
	}
	
	public double ganhouNoAno() {
		return this.salario * 12;
	}
	
	public void horaExtra(double horaExtra) {
		this.salario += horaExtra;
	}
	
	public void monstra() {
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + this.salario);
		System.out.println("Data de entrada: " + this.dataEntrada.getFormatada());
		System.out.println("rg: " + this.rg + "\n");
	}

	public double getSalario() {
		return this.salario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public Data getDataEntrada() {
		return dataEntrada;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public void setDataEntrada(Data dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRg() {
		return rg;
	}

	public static int getIdentificador() {
		return identificador;
	}
}
