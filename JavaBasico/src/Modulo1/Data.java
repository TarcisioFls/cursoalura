package Modulo1;

public class Data {
	
	private int dia;
	private int mes;
	private int ano;
	
	public Data (int dia, int mes, int ano) {
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
		
		if (! isDataViavel(dia, mes, ano)) {
			System.out.println("A data " + getFormatada() + " não é uma data valida");
		}
	}
	
	private boolean isDataViavel ( int dia, int mes, int ano) {
		
		if(dia <= 0 || mes <= 0) {
			return false;
		}
		int ultimoDiaMes = 31;
		if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			ultimoDiaMes = 30;
		} else if (mes == 2) {
			if (ano % 4 == 0) {
				ultimoDiaMes = 29;
			} else {
				ultimoDiaMes = 28;
			}
		}
		if (dia > ultimoDiaMes) {
			return false;
		}
		
		return true;
	}
	
	String getFormatada() {
		String dataFormantada = dia + "/" + mes + "/" + ano;
		return dataFormantada;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
}