package Modulo6;


public class TestarString {
	
	public static void main(String[] args) {
		
		String s = "fj11";
		s = s.replaceAll("1", "2");
		System.out.println(s);
		
		if (s.contains("22")) {
			System.out.println("Possui o número 22");
		} else {
			System.out.println("Não possui o número 22");
		}
		
		String s2 = "Removendo Os Espaços da String";
		s2 = s2.trim();
		
		System.out.println(s2);
		
		if(!s.isEmpty()) {
			System.out.println("Não está vazia");
		} else {
			System.out.println("Está vazia");
		}
		
		System.out.println("A quantidade de caracteres na variavel s2 é " + s2.length());
		
		String s3 = "Socorram-me, subi no ônibus em Marrocos";
		String s4 = "anotaram a data da maratona";
		for  (int i = s3.length() - 1; i >= 0; i--) {
			System.out.print(s3.charAt(i));
		}
		System.out.println();
		
		String[] s5 = s3.split(" ");
		for (int i = s3.split(" ").length - 1; i >= 0; i--) {
			System.out.print(s5[i] + " ");
		}
		
		System.out.println();
		
		StringBuilder s6 = new StringBuilder();
		s6.append(s3);
		System.out.println(s6.reverse());
		
		String s7 = "123";
		int resultado = 0;
        while (s7.length() > 0) {
            char algarismo = s7.charAt(0);
            resultado = resultado * 10 + (algarismo - '0');
            s7 = s7.substring(1);
        }
        System.out.println(resultado);
		
	}

}
