package Modulo6;

public class TesteErro {
	
	public static void main(String[] args) {
		System.out.println("Inicio do main");
		try {
		metodo1();
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Erro");
		}
		System.out.println("Fim do Main");
	}

	private static void metodo1() {
		System.out.println("Inicio do método 1");
		metodo2();
		System.out.println("Fim método 1");
	}

	private static void metodo2() {
		System.out.println("Inicio método 2");
		int[] array = new int[10];
		for(int i = 0; i <= 15;i++) {
			array[i] = i;
			System.out.println(array[i]);
		}
		System.out.println("FIm método 2");
		
	}

}
