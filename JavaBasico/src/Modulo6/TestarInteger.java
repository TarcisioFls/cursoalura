package Modulo6;

import java.io.PrintStream;

public class TestarInteger {
	
	public static void main(String[] args) {
		
		Integer x1 = new Integer(5);
		Integer x2 = new Integer(5);
		
		if(x1 == x2) {
			System.out.println("São iguais");
		} else {
			System.out.println("São diferentes");
		}
		
		if (x1.equals(x2)) {
			System.out.println("São Iguais");
		} else {
			System.out.println("São Diferentes");
		}
		
		String x3 = "33";
		Integer x4;
		
		x4 = Integer.parseInt(x3);
		
		System.out.println(x4);
		
		PrintStream sainda = System.out;
		sainda.println("tutis");
	}

}
