package Modulo8;

import java.util.Collection;

public class ProduzirMensagens implements Runnable{

	private int comeco;
	private int fim;
	private Collection<String> mensagens;
	
	public ProduzirMensagens (int comeco, int fim, Collection<String> mensagens) {
		this.comeco = comeco;
		this.fim = fim;
		this.mensagens = mensagens;
	}
	
	@Override
	public void run() {

		for (int i = comeco; i < fim; i++) {
//			synchronized (mensagens) {
				mensagens.add("Mensagem " + i);
//			}
		}
		
	}

	public int getComeco() {
		return comeco;
	}

	public void setComecoo(int comeco) {
		this.comeco = comeco;
	}

	public int getFim() {
		return fim;
	}

	public void setFim(int fim) {
		this.fim = fim;
	}

	public Collection<String> getMensagens() {
		return mensagens;
	}

	public void setMensagens(Collection<String> mensagens) {
		this.mensagens = mensagens;
	}

}
