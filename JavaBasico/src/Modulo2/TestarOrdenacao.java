package Modulo2;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TestarOrdenacao {
	
	public static void main(String[] args) {
		
		ContaPoupanca c1 = new ContaPoupanca();
		c1.deposita(500);
		c1.setNumero(4);
		c1.setNomeDoCliente("Maria");
		
		ContaPoupanca c2 = new ContaPoupanca();
		c2.deposita(600);
		c2.setNumero(1);
		c2.setNomeDoCliente("Jose");
		
		ContaPoupanca c3 = new ContaPoupanca();
		c3.deposita(800);
		c3.setNumero(2);
		c3.setNomeDoCliente("Joana");
		
		List<ContaPoupanca> contas = new LinkedList<ContaPoupanca>();
		contas.add(c1);
		contas.add(c2);
		contas.add(c3);
		
		Random random =  new Random();
		
		String[] nomes = new String[10];
		nomes[0] = "Will";
		nomes[1] = "João";
		nomes[2] = "Ane";
		nomes[3] = "Gabi";
		nomes[4] = "Andreia";
		nomes[5] = "Camila";
		nomes[6] = "Juliana";
		nomes[7] = "Gustavo";
		nomes[8] = "Zain";
		nomes[9] = "Ain";
				
		
		for (int i = 1; i <= 10;i++) {
			 ContaPoupanca c = new ContaPoupanca();
			 c.setNumero(random.nextInt(50));
			 c.deposita(random.nextInt(1000) * random.nextDouble());
			 c.setNomeDoCliente(nomes[random.nextInt(9)]);
			 contas.add(c);
		}
		
		Collections.sort(contas);
		
		for (ContaPoupanca poupanca : contas) {
			System.out.print("Numero: " + poupanca.getNumero() + " ");
			System.out.print(poupanca.getNomeDoCliente() + " ");
			System.out.format("R$ %.2f", poupanca.getSaldo());
			System.out.println();
		}
	}

}
