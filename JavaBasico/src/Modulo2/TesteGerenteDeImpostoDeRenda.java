package Modulo2;

public class TesteGerenteDeImpostoDeRenda {
	
	public static void main(String[] args) {
		
		GerenteDeImpostoDeRenda gerenciador = new GerenteDeImpostoDeRenda();
		
		SeguroDeVida sv = new SeguroDeVida();
		
		gerenciador.adiciona(sv);
		
		ContaCorrente cc = new ContaCorrente();
		cc.deposita(1000);
		gerenciador.adiciona(cc);
		
		System.out.printf("O total é: %.2f", gerenciador.getTotal());
		
	}

}
