package Modulo2;

public class TestarComparacaoConta {
	
	public static void main(String[] args) {
		
		Conta c1 = new ContaCorrente();
		Conta c2 = new ContaCorrente();
		
		c1.setNumero(123);
		c2.setNumero(123);
		
		if (c1 == c2) {
			System.out.println("São iguais");
		} else {
			System.out.println("São diferentes");
		}
		
		if (c1.equals(c2)) {
			System.out.println("SÃO IGUAIS");
		} else {
			System.out.println("SÃO DIFERENTES");
		}
		
	}

}
