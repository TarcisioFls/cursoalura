package Modulo2;

public class ContaPoupanca extends Conta implements  Comparable<ContaPoupanca>{
	
	private String nomeDoCliente;
	
	public void deposita(double valorASerDepositado) {
		this.saldo += valorASerDepositado;
		this.saldo -= 0.10;
	}

	@Override
	public void atualiza(double taxa) {
		this.saldo += (saldo * taxa) * 3;
		
	}

	@Override
	public int compareTo(ContaPoupanca outra) {
		return this.nomeDoCliente.compareTo(outra.nomeDoCliente);
//		if(this.getNumero() < outra.getNumero()) {
//			return -1;
//		} else if (this.getNumero() > outra.getNumero()) {
//			return 1;
//		}
//		return 0;
	}

	public String getNomeDoCliente() {
		return nomeDoCliente;
	}

	public void setNomeDoCliente(String nomeDoCliente) {
		this.nomeDoCliente = nomeDoCliente;
	}

}
