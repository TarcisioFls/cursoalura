package Modulo2;

public abstract class Conta {
	
	protected double saldo = 0;
	protected int numero;
	private String nome;
	
	public void deposita( double valorASerDepositado){
		if (valorASerDepositado < 0) {
			throw new ValorInvalidoException(valorASerDepositado);
		} else {
		this.saldo += valorASerDepositado;
		}
	}
	
	public void sacar(double valorASerSacado) {
		this.saldo -= valorASerSacado;
	}
	
	public abstract void atualiza(double taxa);
	
	public int getNumero() {
		return numero;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Esse objeto é uma conta com o salto: " + this.saldo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numero;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (numero != other.numero)
			return false;
		return true;
	}

//	@Override
//	public boolean equals(Object obj) {
//		Conta outraConta = (Conta) obj;
//		if(!(outraConta.numero == this.numero) && outraConta.getNome() == this.getNome()) {
//			return false;
//		} else {
//			return true;
//		}
//	}
}