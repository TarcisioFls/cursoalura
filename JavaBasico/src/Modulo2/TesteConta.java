package Modulo2;


public class TesteConta {

	public static void main(String[] args) {
		ContaPoupanca contaPoupanca = new ContaPoupanca();
		ContaCorrente contaCorrente = new ContaCorrente();
		AtualizadorDeContas atualizadorDeContas = new AtualizadorDeContas(0.01);
		
		System.out.println("Poupança");
		contaPoupanca.deposita(200);
		System.out.println("Valor na Poupança: " + contaPoupanca.getSaldo());
		contaPoupanca.sacar(100);
		System.out.println("Valor na Poupança: " + contaPoupanca.getSaldo());
		contaPoupanca.atualiza(0.10);
		System.out.println("Valor na Poupança: " + contaPoupanca.getSaldo());
		System.out.println("");
		System.out.println("FIM");
		System.out.println("");
		
		System.out.println("Corrente");
		contaCorrente.deposita(200);
		System.out.println("Valor na Corrente: " + contaCorrente.getSaldo());
		contaCorrente.sacar(100);
		System.out.println("Valor na Corrente: " + contaCorrente.getSaldo());
		contaCorrente.atualiza(0.10);
		System.out.println("Valor na Corrente: " + contaCorrente.getSaldo());
		System.out.println("");
		System.out.println("FIM");
		System.out.println("");
		
		System.out.println("Atualizador de Conta Corrente");
		atualizadorDeContas.rodar(contaCorrente);
		atualizadorDeContas.getSaldoTotal();
		System.out.println("");
		System.out.println("FIM");
		System.out.println("");
		
		System.out.println("Atualizador de Conta Poupança");
		atualizadorDeContas.rodar(contaPoupanca);
		atualizadorDeContas.getSaldoTotal();
		System.out.println("");
		System.out.println("FIM");
		System.out.println("");
		
		System.out.println("Saldo Total: "
				+ atualizadorDeContas.getSaldoTotal());
		
		Conta c1 = new ContaCorrente();
		c1.deposita(100);
		System.out.println(c1);

	}

}
