package Modulo2;

public class AtualizadorDeContas {
	private double saldoTotal = 0;
	private double selic;
	
	public AtualizadorDeContas (double selic) {
		this.selic += selic;
	}
	
	public void rodar(Conta conta) {
		System.out.println("Saldo Atual: " + conta.getSaldo());
		conta.atualiza(this.selic);
		System.out.println("Saldo atualizado com a taxa Selic: " + conta.getSaldo());
		this.saldoTotal += conta.getSaldo();
	}
	
	public double getSaldoTotal() {
		return this.saldoTotal;
	}
}